package fragments.android.m_commerce.com.wa;

/**
 * Created by Andrew on 2015-05-08.
 */
public class Weather {

    public float temperature;
    public int humidity;
    public int pressure;
    public String icon;
    public String city;
    public String country;

    public byte[] iconData;


}
