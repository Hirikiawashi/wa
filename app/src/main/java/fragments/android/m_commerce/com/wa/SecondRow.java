package fragments.android.m_commerce.com.wa;

/**
 * Created by Andrew on 2015-05-08.
 */

import java.util.ArrayList;
import java.util.List;

public class SecondRow {

    public void GetCity(List<String> list, String country) {

        int i = 0;

        ThirdRow cities = new ThirdRow();
        FirstRow continents = new FirstRow();

        continents.RestartList(list);

        List<String> tmp = new ArrayList<String>();

        if(country.equals(" Poland")) {
            continents.GetCountry(list, "EUROPE");
            tmp = cities.Cities(0);
        }
        else if(country.equals(" Germany")) {
            continents.GetCountry(list, "EUROPE");
            tmp = cities.Cities(1);
        }
        else if(country.equals(" France")) {
            continents.GetCountry(list, "EUROPE");
            tmp = cities.Cities(2);
        }
        else if(country.equals(" Japane")) {
            continents.GetCountry(list, "ASIA");
            tmp = cities.Cities(3);
        }
        else if(country.equals(" United States")) {
            continents.GetCountry(list, "N. AMERICA");
            tmp = cities.Cities(4);
        }

        i = list.indexOf(country);
        System.out.println(i);

        if(!tmp.isEmpty()){
            for(String s : tmp) {
                System.out.println(s);
                list.add(++i, s);
            }
        }

    }
}

