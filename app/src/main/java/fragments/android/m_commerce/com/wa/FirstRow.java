package fragments.android.m_commerce.com.wa;

/**
 * Created by Andrew on 2015-05-08.
 */

import java.util.ArrayList;
import java.util.List;

public class FirstRow {

    public FirstRow() {

    }

    public List<String> GenerateContinents(){

        List<String> continents = new ArrayList();

        continents.add("AFRICA");
        continents.add("ASIA");
        continents.add("EUROPE");
        continents.add("N. AMERICA");
        continents.add("S. AMERICA");

        return continents;
    }

            public void RestartList(List<String> list) {

                list.clear();
                list.add("AFRICA");
                list.add("ASIA");
                list.add("EUROPE");
                list.add("N. AMERICA");
                list.add("S. AMERICA");
            }

    public void GetCountry(List<String> list, String continent) {

        RestartList(list);
        int i = list.indexOf(continent);

        if(continent.equals("AFRICA")) {
            list.add(i+1," Egipt");
            list.add(i+2," Burkina Faso");
            list.add(i+3," Kenya");
            list.add(i+4," Ivory Coast");
        }
        else if(continent.equals("ASIA")) {
            list.add(i+1," China");
            list.add(i+2," India");
            list.add(i+3," Japan");
        }
        else if(continent.equals("EUROPE")) {
            list.add(i+1," Poland");
            list.add(i+2," Germany");
            list.add(i+3," France");
            list.add(i+4," Norway");
        }
        else if(continent.equals("N. AMERICA")) {
            list.add(i+1," Canada");
            list.add(i+2," Mexico");
            list.add(i+3," United States");
            list.add(i+4," Bermuda");
        }
        else if(continent.equals("S. AMERICA")) {
            list.add(i+1," Argentina");
            list.add(i+2," Brazil");
            list.add(i+3," Chile");
            list.add(i+4," Colombia");
        }


    }


}

