package fragments.android.m_commerce.com.wa;

/**
 * Created by Andrew on 2015-05-08.
 */

import java.util.ArrayList;
import java.util.List;

public class ThirdRow {

    public List<ArrayList<String>> ArrayCities = new ArrayList<ArrayList<String>>();

    public List<String> Cities(int i){

        ArrayList<String> tmp = new ArrayList<String>();

        if(i == 0) {
            tmp.add("   Gdynia");
            tmp.add("   Warszawa");
            tmp.add("   Krakow");
            tmp.add("   Lublin");
        }
        else if(i == 1){
            tmp.add("   Berlin");
            tmp.add("   Frankfurt");
        }
        else if(i == 2){
            tmp.add("   New York");
            tmp.add("   Detroit");
        }
        else if(i == 3){
            tmp.add("   Sapporo");
        }
        else if(i == 4){
            tmp.add("   New York");
        }

        return tmp;

    }

    public List<String> getCities(){

        List<String> tmp = new ArrayList<String>();
        List<String> cities = new ArrayList<String>();

        for(int i = 0; i < 4; i++) {

            tmp = Cities(i);

            for(String s : tmp) {
                cities.add(s);
            }

        }

        return cities;
    }
}

